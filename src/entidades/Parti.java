package entidades;

public class Parti {

	public static final Parti ALUNO = null;
	public static final Parti ORIENTADOR = null;
	public static final Parti CONVIDADO = null;
	private Parti tipo;
	private Pessoa pessoa;
	private Nota nota;

	public Parti() {

	}

	public Parti(Parti Tipo, Pessoa Pessoa) {
		this.tipo = Tipo;
		this.pessoa = Pessoa;
	}

	public Parti(Parti Tipo, Pessoa Pessoa, Nota Nota) {
		this.tipo = Tipo;
		this.pessoa = Pessoa;
		this.nota = Nota;
	}

	public Parti getTipo() {
		return tipo;
	}

	public void setTipo(Parti tipo) {
		this.tipo = tipo;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		pessoa = pessoa;
	}

	public Nota getNota() {
		return nota;
	}

	public void setNota(Nota nota) {
		this.nota = nota;
	}

	@Override
	public String toString() {
		return tipo.Descricao() + ": " + pessoa.getNome() + " nota: " + nota + "\n";
	}

	private String Descricao() {
		// TODO Auto-generated method stub
		return null;
	}

}
