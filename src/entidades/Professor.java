package entidades;


public class Professor extends Pessoa {

	private Integer anoIngresso;

	public Professor() {
		super();
	}

	public Professor(String nome, String email, String telefone, Integer anoIngresso) {
		super(nome, email, telefone);
		this.anoIngresso = anoIngresso;
	}

	public Integer getAnoIngresso() {
		return anoIngresso;
	}

	public void setAnoIngresso(Integer anoIngresso) {
		this.anoIngresso = anoIngresso;
	}

	@Override
	public String toString() {
		return super.toString() + " ingresso: " + anoIngresso;
	}

}
