package entidades;

import Enum.TrabalhoSit;

public class Trabalho {

	private String titulo;
	private TrabalhoSit situacao = TrabalhoSit.EM_ABERTO;
	private Parti listaEnvolvidos[];
	private String dataApresentacao;
	private double notaFinal;

	public Trabalho() {
	}

	public Trabalho(String titulo, Parti[] listaParti) {
		super();
		this.titulo = titulo;
		this.listaEnvolvidos = listaParti;
	}

	public Trabalho(String titulo, Parti[] listaParti, String dataApresentacao, double notaFinal) {
		super();
		this.titulo = titulo;
		this.listaEnvolvidos = listaParti;
		this.dataApresentacao = dataApresentacao;
		this.notaFinal = notaFinal;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Parti[] getListaEnvolvidos() {
		return listaEnvolvidos;
	}

	public void setListaParti(Parti[] listaParti) {
		this.listaEnvolvidos = listaParti;
	}

	public String getDataApresentacao() {
		return dataApresentacao;
	}

	public TrabalhoSit getSituacao() {
		return situacao;
	}

	public void setDataApresentacao(String dataApresentacao) {
		this.dataApresentacao = dataApresentacao;
	}

	public double getNotaFinal() {
		return notaFinal;
	}

	public void setNotaFinal(double notaFinal) {
		this.notaFinal = notaFinal;
	}

	public void setSituacao(TrabalhoSit Situacao) {
		this.situacao = Situacao;
	}

	@Override
	public String toString() {
		return titulo + "\nApresentacao: " + dataApresentacao + "\n NotaFinal: " + notaFinal + " - situacao: "
				+ situacao;
	}

	public Parti[] getListaParti() {
		return listaEnvolvidos;
	}

}
