package entidades;


import Enum.AlunoSit;

public class Aluno extends Pessoa {

	private Integer anoIngresso;
	private Integer anoConclusao;
	private Banca curso;
	private AlunoSit situacao;

	public Aluno() {
		super();
	}

	public Aluno(String nome, String email, String telefone, Integer anoIngresso, Integer anoConclusao, Banca curso) {
		super(nome, email, telefone);
		this.anoIngresso = anoIngresso;
		this.anoConclusao = anoConclusao;
		this.curso = curso;
	}

	public Integer getAnoIngresso() {
		return anoIngresso;
	}

	public void setAnoIngresso(Integer anoIngresso) {
		this.anoIngresso = anoIngresso;
	}

	public Integer getAnoConclusao() {
		return anoConclusao;
	}

	public AlunoSit getSituacao() {
		return situacao;
	}

	public void setAnoConclusao(Integer anoConclusao) {
		this.anoConclusao = anoConclusao;
	}

	public Banca getCurso() {
		return curso;
	}

	public void setCurso(Banca curso) {
		this.curso = curso;
	}

	public void setSituacao(AlunoSit situacao) {
		this.situacao = situacao;
	}

	@Override
	public String toString() {
		return "Aluno: " + super.toString() + "Ingresso em " + anoIngresso + " e conclus�o " + anoConclusao + " do curso " + curso + "\n";
	}
		
}
