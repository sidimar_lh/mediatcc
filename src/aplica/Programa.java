package aplica;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import Enum.AlunoSit;
import Enum.TrabalhoSit;
import entidades.Aluno;
import entidades.Banca;
import entidades.Nota;
import entidades.Parti;
import entidades.Pessoa;
import entidades.Professor;
import entidades.Trabalho;

public class Programa {

	private JFrame janela;

	ArrayList<Banca> ListaCurso;
	ArrayList<Aluno> ListaAluno;
	ArrayList<Professor> ListaProfessor;
	ArrayList<Trabalho> ListaTrabalho;

	private JTextField txtCadastroCod;
	private JTextField txtCadastroDsc;
	private JTextField txtPessoaNom;
	private JTextField txtPessoaEml;
	private JTextField txtPessoatel;
	private JTextField txtPessoaIng;
	private JTextField txtPessoaCon;
	private JTextField txtTrabalhoTit;
	private JTextField txtBancaOrn;
	private JTextField txtBancaAln;
	private JTextField txtBancaNotaA;
	private JTextField txtBancaNotaO;
	private JTextField txtBancaNotaP1;
	private JTextField txtBancaNotaP2;
	private JTextField txtBancaNotaFinal;
	private JComboBox<String> CmbTrabalhoAln = null;
	private JComboBox<String> CmbTrabalhoOrn = null;
	private JComboBox<String> CmbBancaTrb = null;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Programa window = new Programa();
					window.janela.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Programa() {
		initialize();

		ListaCurso = new ArrayList<Banca>();
		ListaAluno = new ArrayList<Aluno>();
		ListaProfessor = new ArrayList<Professor>();
		ListaTrabalho = new ArrayList<Trabalho>();
	}

	private Pessoa GetPessoa(Parti Tipo, Parti ListaPessoas[]) {
		for (int i = 0; i < ListaPessoas.length; i++) {
			System.out.println(ListaPessoas[i].getTipo());
			if (ListaPessoas[i].getTipo() == Tipo)
				return ListaPessoas[i].getPessoa();
		}

		return null;
	}

	private Double CalcularNotaFinal(Parti ListaEnvolvidos[]) {
		Double Resultado = 0.0;

		for (int i = 0; i < ListaEnvolvidos.length; i++) {
			Resultado += (ListaEnvolvidos[i].getNota().getNota() * ListaEnvolvidos[i].getNota().getPeso());
		}

		return Resultado;
	}

	public String getDataAtual() {
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		Date dataAtual = new Date(System.currentTimeMillis());

		return formato.format(dataAtual);
	}

	private void initialize() {
		janela = new JFrame();
		janela.getContentPane().setBackground(new Color(135, 206, 235));
		janela.setResizable(false);
		janela.setTitle("Calculo TCC");
		janela.setBounds(100, 100, 463, 302);
		janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		janela.getContentPane().setLayout(null);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {

			}
		});
		tabbedPane.setBounds(10, 11, 425, 239);
		janela.getContentPane().add(tabbedPane);

		JPanel abaCadastro = new JPanel();
		abaCadastro.setBackground(new Color(230, 230, 250));
		tabbedPane.addTab("Cadastro", null, abaCadastro, null);
		abaCadastro.setLayout(null);

		JLabel lblCadastroCod = new JLabel("        C\u00F3digo:");
		lblCadastroCod.setBounds(10, 15, 70, 14);
		abaCadastro.add(lblCadastroCod);

		txtCadastroCod = new JTextField();
		txtCadastroCod.setBounds(80, 10, 86, 20);
		abaCadastro.add(txtCadastroCod);
		txtCadastroCod.setColumns(10);

		JLabel lblCadastroDsc = new JLabel("  Descri\u00E7\u00E3o:");
		lblCadastroDsc.setBounds(10, 40, 70, 14);
		abaCadastro.add(lblCadastroDsc);

		txtCadastroDsc = new JTextField();
		txtCadastroDsc.setBounds(80, 35, 320, 20);
		abaCadastro.add(txtCadastroDsc);
		txtCadastroDsc.setHorizontalAlignment(SwingConstants.LEFT);
		txtCadastroDsc.setColumns(30);

		JPanel abaPessoa = new JPanel();
		abaPessoa.setBackground(new Color(230, 230, 250));
		tabbedPane.addTab("Pessoa", null, abaPessoa, null);
		abaPessoa.setLayout(null);

		JComboBox CmbPessoaCur = new JComboBox();
		CmbPessoaCur.setBounds(80, 66, 320, 20);
		abaPessoa.add(CmbPessoaCur);

		JLabel lblPessoaCur = new JLabel("         Curso:");
		lblPessoaCur.setBounds(10, 69, 70, 14);
		abaPessoa.add(lblPessoaCur);

		txtPessoaNom = new JTextField();
		txtPessoaNom.setBounds(80, 10, 320, 20);
		abaPessoa.add(txtPessoaNom);
		txtPessoaNom.setColumns(10);

		txtPessoaEml = new JTextField();
		txtPessoaEml.setBounds(80, 35, 320, 20);
		abaPessoa.add(txtPessoaEml);
		txtPessoaEml.setColumns(10);

		txtPessoatel = new JTextField();
		txtPessoatel.setBounds(80, 183, 86, 20);
		abaPessoa.add(txtPessoatel);
		txtPessoatel.setColumns(10);

		JLabel lblPessoaNom = new JLabel("          Nome:");
		lblPessoaNom.setBounds(10, 15, 70, 14);
		abaPessoa.add(lblPessoaNom);

		JLabel lblPessoaEml = new JLabel("           Email:");
		lblPessoaEml.setBounds(10, 40, 70, 14);
		abaPessoa.add(lblPessoaEml);

		JLabel lblPessoaTel = new JLabel("     Telefone:");
		lblPessoaTel.setBounds(10, 186, 70, 14);
		abaPessoa.add(lblPessoaTel);

		JLabel lblPessoaTip = new JLabel("            Tipo:");
		lblPessoaTip.setBounds(10, 94, 70, 14);
		abaPessoa.add(lblPessoaTip);

		JRadioButton rdbPessoaAln = new JRadioButton("Aluno");
		rdbPessoaAln.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if (rdbPessoaAln.isSelected()) {
					CmbPessoaCur.setEnabled(true);
					txtPessoaCon.setEnabled(true);
				} else {
					CmbPessoaCur.setSelectedIndex(-1);
					txtPessoaCon.setText("");

					CmbPessoaCur.setEnabled(false);
					txtPessoaCon.setEnabled(false);
				}
			}
		});
		buttonGroup.add(rdbPessoaAln);
		rdbPessoaAln.setBounds(80, 93, 60, 23);
		abaPessoa.add(rdbPessoaAln);

		JRadioButton rdbPessoaPrf = new JRadioButton("Professor");
		buttonGroup.add(rdbPessoaPrf);
		rdbPessoaPrf.setBounds(140, 93, 100, 23);
		abaPessoa.add(rdbPessoaPrf);

		txtPessoaIng = new JTextField();
		txtPessoaIng.setBounds(80, 119, 86, 20);
		abaPessoa.add(txtPessoaIng);
		txtPessoaIng.setColumns(10);

		JLabel lblPessoaIng = new JLabel("     Ingresso:");
		lblPessoaIng.setBounds(10, 118, 70, 14);
		abaPessoa.add(lblPessoaIng);

		txtPessoaCon = new JTextField();
		txtPessoaCon.setBounds(80, 150, 86, 20);
		abaPessoa.add(txtPessoaCon);
		txtPessoaCon.setColumns(10);

		JLabel lblPessoaCon = new JLabel(" Conclus\u00E3o:");
		lblPessoaCon.setBounds(10, 150, 70, 14);
		abaPessoa.add(lblPessoaCon);

		JButton btnPessoaSlv = new JButton("Salvar");
		btnPessoaSlv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index = CmbPessoaCur.getSelectedIndex();

				if (rdbPessoaAln.isSelected()) {
					Aluno aluno = new Aluno(txtPessoaNom.getText(), txtPessoaEml.getText(), txtPessoatel.getText(),
							Integer.parseInt(txtPessoaIng.getText()), Integer.parseInt(txtPessoaCon.getText()),
							ListaCurso.get(index));

					ListaAluno.add(aluno);

					CmbTrabalhoAln.removeAllItems();
					for (int i = 0; i < ListaAluno.size(); i++)
						CmbTrabalhoAln.addItem(ListaAluno.get(i).getNome());
				} else {
					Professor professor = new Professor(txtPessoaNom.getText(), txtPessoaEml.getText(),
							txtPessoatel.getText(), Integer.parseInt(txtPessoaIng.getText()));

					ListaProfessor.add(professor);
					CmbTrabalhoOrn.removeAllItems();
					for (int i = 0; i < ListaProfessor.size(); i++)
						CmbTrabalhoOrn.addItem(ListaProfessor.get(i).getNome());
				}

				CmbTrabalhoAln.setSelectedIndex(-1);
				CmbTrabalhoOrn.setSelectedIndex(-1);
				txtPessoaNom.setText("");
				txtPessoaEml.setText("");
				txtPessoatel.setText("");
				txtPessoaIng.setText("");
				txtPessoaCon.setText("");
				CmbPessoaCur.setSelectedIndex(-1);
				rdbPessoaAln.setSelected(false);
				rdbPessoaAln.setSelected(false);
			}
		});
		btnPessoaSlv.setBounds(311, 185, 89, 23);
		abaPessoa.add(btnPessoaSlv);

		JPanel abaTrabalho = new JPanel();
		abaTrabalho.setBackground(new Color(230, 230, 250));
		tabbedPane.addTab("Trabalho", null, abaTrabalho, null);
		abaTrabalho.setLayout(null);

		CmbTrabalhoAln = new JComboBox();
		CmbTrabalhoAln.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {

			}
		});
		CmbTrabalhoAln.setBounds(80, 10, 320, 20);
		abaTrabalho.add(CmbTrabalhoAln);

		JLabel lblTrabalhoAln = new JLabel("          Aluno:");
		lblTrabalhoAln.setBounds(10, 15, 70, 14);
		abaTrabalho.add(lblTrabalhoAln);

		JLabel lblTrabalhoTit = new JLabel("          T\u00EDtulo:");
		lblTrabalhoTit.setBounds(10, 40, 70, 14);
		abaTrabalho.add(lblTrabalhoTit);

		txtTrabalhoTit = new JTextField();
		txtTrabalhoTit.setBounds(80, 35, 320, 20);
		abaTrabalho.add(txtTrabalhoTit);
		txtTrabalhoTit.setColumns(10);

		JLabel lblPessoaOrn = new JLabel("Orientador:");
		lblPessoaOrn.setBounds(10, 65, 70, 14);
		abaTrabalho.add(lblPessoaOrn);

		CmbTrabalhoOrn = new JComboBox();
		CmbTrabalhoOrn.setBounds(80, 60, 320, 20);
		abaTrabalho.add(CmbTrabalhoOrn);

		JPanel abaBanca = new JPanel();
		abaBanca.setBackground(new Color(230, 230, 250));
		tabbedPane.addTab("Banca", null, abaBanca, null);
		abaBanca.setLayout(null);

		JLabel lblBancaTrb = new JLabel("    Trabalho:");
		lblBancaTrb.setBounds(10, 15, 70, 14);
		abaBanca.add(lblBancaTrb);

		JLabel lblBancaAln = new JLabel("         Aluno:");
		lblBancaAln.setBounds(10, 40, 70, 14);
		abaBanca.add(lblBancaAln);

		JLabel lblBancaOrn = new JLabel("Orientador:");
		lblBancaOrn.setBounds(10, 65, 70, 14);
		abaBanca.add(lblBancaOrn);

		txtBancaOrn = new JTextField();
		txtBancaOrn.setEditable(false);
		txtBancaOrn.setBounds(80, 60, 230, 20);
		abaBanca.add(txtBancaOrn);
		txtBancaOrn.setColumns(10);

		txtBancaAln = new JTextField();
		txtBancaAln.setEditable(false);
		txtBancaAln.setBounds(80, 35, 230, 20);
		abaBanca.add(txtBancaAln);
		txtBancaAln.setColumns(10);

		JLabel lblBancaPrf1 = new JLabel("        Prof. 1:");
		lblBancaPrf1.setBounds(10, 90, 70, 14);
		abaBanca.add(lblBancaPrf1);

		JLabel lblBancaPrf2 = new JLabel("        Prof. 2:");
		lblBancaPrf2.setBounds(10, 115, 70, 14);
		abaBanca.add(lblBancaPrf2);

		JLabel lblBancaNotaA = new JLabel(" Nota:");
		lblBancaNotaA.setBounds(310, 40, 40, 14);
		abaBanca.add(lblBancaNotaA);

		JLabel lblBancaNotaO = new JLabel(" Nota:");
		lblBancaNotaO.setBounds(310, 65, 40, 14);
		abaBanca.add(lblBancaNotaO);

		JLabel lblBancaNotaP1 = new JLabel(" Nota:");
		lblBancaNotaP1.setBounds(310, 90, 40, 14);
		abaBanca.add(lblBancaNotaP1);

		JLabel lblBancaNotaP2 = new JLabel(" Nota:");
		lblBancaNotaP2.setBounds(310, 115, 40, 14);
		abaBanca.add(lblBancaNotaP2);

		txtBancaNotaA = new JTextField();
		txtBancaNotaA.setBounds(350, 37, 50, 20);
		abaBanca.add(txtBancaNotaA);
		txtBancaNotaA.setColumns(10);

		txtBancaNotaO = new JTextField();
		txtBancaNotaO.setBounds(350, 62, 50, 20);
		abaBanca.add(txtBancaNotaO);
		txtBancaNotaO.setColumns(10);

		txtBancaNotaP1 = new JTextField();
		txtBancaNotaP1.setBounds(350, 87, 50, 20);
		abaBanca.add(txtBancaNotaP1);
		txtBancaNotaP1.setColumns(10);

		txtBancaNotaP2 = new JTextField();
		txtBancaNotaP2.setBounds(350, 112, 50, 20);
		abaBanca.add(txtBancaNotaP2);
		txtBancaNotaP2.setColumns(10);

		JComboBox cmbBancaPrf1 = new JComboBox();
		cmbBancaPrf1.setBounds(80, 85, 230, 20);
		abaBanca.add(cmbBancaPrf1);

		JComboBox cmbBancaPrf2 = new JComboBox();
		cmbBancaPrf2.setBounds(80, 110, 230, 20);
		abaBanca.add(cmbBancaPrf2);

		JLabel label = new JLabel("--------------------------------------------------------------------------------------------------------");
		label.setBounds(0, 134, 420, 14);
		abaBanca.add(label);

		JLabel lblNotafinal = new JLabel("   Nota final:");
		lblNotafinal.setBounds(10, 159, 70, 14);
		abaBanca.add(lblNotafinal);

		txtBancaNotaFinal = new JTextField();
		txtBancaNotaFinal.setEnabled(false);
		txtBancaNotaFinal.setEditable(false);
		txtBancaNotaFinal.setBounds(80, 156, 86, 20);
		abaBanca.add(txtBancaNotaFinal);
		txtBancaNotaFinal.setColumns(10);

		JLabel lblBancaSituacao = new JLabel("");
		lblBancaSituacao.setBounds(174, 159, 153, 14);
		abaBanca.add(lblBancaSituacao);

		JButton btnCodigoSlv = new JButton("Salvar");
		btnCodigoSlv.setBounds(311, 185, 89, 23);
		btnCodigoSlv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Banca curso = new Banca(Integer.parseInt(txtCadastroCod.getText()), txtCadastroDsc.getText());

				ListaCurso.add(curso);

				txtCadastroCod.setText("");
				txtCadastroDsc.setText("");

				CmbPessoaCur.removeAllItems();
				for (int i = 0; i < ListaCurso.size(); i++) {
					CmbPessoaCur.addItem(ListaCurso.get(i).getDescricao());
					System.out.println(ListaCurso.get(i).getDescricao());
				}
				
				CmbPessoaCur.setSelectedIndex(-1);
			}
		});
		abaCadastro.add(btnCodigoSlv);

		CmbBancaTrb = new JComboBox();
		CmbBancaTrb.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(tabbedPane.getSelectedIndex() == 3) {
					txtBancaAln.setText("");
					txtBancaOrn.setText("");
					txtBancaNotaA.setText("");
					txtBancaNotaO.setText("");
					txtBancaNotaP1.setText("");
					txtBancaNotaP2.setText("");
					txtBancaNotaFinal.setText("");
	
					lblBancaSituacao.setText("");
	
					cmbBancaPrf1.removeAllItems();
					cmbBancaPrf2.removeAllItems();
	
					if (CmbBancaTrb.getSelectedIndex() >= 0) {
						Trabalho trabalho = ListaTrabalho.get(CmbBancaTrb.getSelectedIndex());
						Parti listaParti[] = trabalho.getListaParti();
						txtBancaAln.setText(GetPessoa(Parti.ALUNO, listaParti).getNome());
	
						Pessoa orientador = GetPessoa(Parti.ORIENTADOR, listaParti);
						txtBancaOrn.setText(orientador.getNome());
	
						if (trabalho.getSituacao() == TrabalhoSit.EM_ABERTO) {
							for (int i = 0; i < ListaProfessor.size(); i++) {
								if (!ListaProfessor.get(i).equals(orientador)) {
									cmbBancaPrf1.addItem(ListaProfessor.get(i).getNome());
									cmbBancaPrf2.addItem(ListaProfessor.get(i).getNome());
								}
							}
	
							cmbBancaPrf1.setSelectedIndex(-1);
							cmbBancaPrf2.setSelectedIndex(-1);
						} else {
							for (int i = 0; i < ListaProfessor.size(); i++) {
								if (!ListaProfessor.get(i).equals(orientador)) {
									if (cmbBancaPrf1.getSelectedIndex() == -1)
										cmbBancaPrf1.addItem(ListaProfessor.get(i).getNome());
									else
										cmbBancaPrf2.addItem(ListaProfessor.get(i).getNome());
								}
							}
	
							txtBancaNotaFinal.setText(String.valueOf(trabalho.getNotaFinal()));
							if (trabalho.getNotaFinal() > 7.0) {
								lblBancaSituacao.setForeground(Color.GREEN);
								lblBancaSituacao.setText(AlunoSit.APROVADO.Descricao());
							} else {
								lblBancaSituacao.setForeground(Color.RED);
								lblBancaSituacao.setText(AlunoSit.REPROVADO.Descricao());
							}
	
						}
					}
				}
			}
		});
		CmbBancaTrb.setBounds(80, 10, 320, 20);
		abaBanca.add(CmbBancaTrb);

		JButton btnBancaCalc = new JButton("Calcular");
		btnBancaCalc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Trabalho trabalho = ListaTrabalho.get(CmbBancaTrb.getSelectedIndex());
				Parti listaParti[] = trabalho.getListaParti();

				listaParti[2] = new Parti(Parti.CONVIDADO, null);
				listaParti[3] = new Parti(Parti.CONVIDADO, null);

				listaParti[0].setNota(new Nota(Double.parseDouble(txtBancaNotaA.getText()), 0.2));
				listaParti[1].setNota(new Nota(Double.parseDouble(txtBancaNotaO.getText()), 0.3));
				listaParti[2].setNota(new Nota(Double.parseDouble(txtBancaNotaP1.getText()), 0.25));
				listaParti[3].setNota(new Nota(Double.parseDouble(txtBancaNotaP2.getText()), 0.25));

				trabalho.setNotaFinal(CalcularNotaFinal(listaParti));
				trabalho.setDataApresentacao(getDataAtual());

				txtBancaNotaFinal.setText(String.valueOf(trabalho.getNotaFinal()));

				if (trabalho.getNotaFinal() > 7.0) {

					lblBancaSituacao.setForeground(Color.GREEN);
					lblBancaSituacao.setText(AlunoSit.APROVADO.Descricao());
				} else {
					lblBancaSituacao.setForeground(Color.RED);
					lblBancaSituacao.setText(AlunoSit.REPROVADO.Descricao());
				}
				trabalho.setSituacao(TrabalhoSit.FINALIZADO);
			}
		});
		btnBancaCalc.setBounds(311, 184, 89, 23);
		abaBanca.add(btnBancaCalc);

		JButton btnTrabalhoSlv = new JButton("Salvar");
		btnTrabalhoSlv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Parti listaParti[] = new Parti[4];

				int index = CmbTrabalhoAln.getSelectedIndex();
				listaParti[0] = new Parti(Parti.ALUNO, ListaAluno.get(index));

				index = CmbTrabalhoOrn.getSelectedIndex();
				listaParti[1] = new Parti(Parti.ORIENTADOR, ListaProfessor.get(index));

				Trabalho trabalho = new Trabalho(txtTrabalhoTit.getText(), listaParti);

				ListaTrabalho.add(trabalho);

				CmbBancaTrb.removeAllItems();
				for (int i = 0; i < ListaTrabalho.size(); i++) {
					CmbBancaTrb.addItem(ListaTrabalho.get(i).getTitulo());
				}
				CmbBancaTrb.setSelectedIndex(-1);

				txtTrabalhoTit.setText("");
				CmbTrabalhoAln.setSelectedIndex(-1);
				CmbTrabalhoOrn.setSelectedIndex(-1);
			}
		});
		btnTrabalhoSlv.setBounds(311, 185, 89, 23);
		abaTrabalho.add(btnTrabalhoSlv);
	}
}
