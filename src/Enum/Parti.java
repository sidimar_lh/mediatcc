package Enum;

public enum Parti {
	ALUNO(0, "Aluno"), ORIENTADOR(1, "Prof. orientador"), CONVIDADO(2, "Prof. convidado"); 
	
	private int codigo;
	private String descricao;
	
	Parti(int Codigo, String Descricao) {
		this.codigo = Codigo;
		this.descricao = Descricao;
	}
	
	public String Descricao() {
		return descricao;
	}
}
