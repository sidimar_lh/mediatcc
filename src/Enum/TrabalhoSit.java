package Enum;

public enum TrabalhoSit {
	EM_ABERTO("Em aberto"), FINALIZADO("Finalizado");
	
	private String descricao;
	
	TrabalhoSit(String Descricao) {
		this.descricao = Descricao;
	}
	
	public String Descricao() {
		return descricao;
	}
}