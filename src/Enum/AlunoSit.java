package Enum;

public enum AlunoSit {
	REPROVADO("Reprovado"), APROVADO("Aprovado");
	
	private String descricao;
	
	private AlunoSit(String Descricao) {
		this.descricao = Descricao;
	}
	
	public String Descricao() {
		return this.descricao;
	}
}
